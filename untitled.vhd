library ieee ;
	use ieee.std_logic_1164.all ;
	use ieee.numeric_std.all ;

entity shifter is
  port (
  	input : in std_logic_vector(7 downto 0);
  	cnt : in std_logic_vector(1 downto 0);
  	result : out std_logic_vector(15 downto 0)
  ) ;
end shifter ; -- 

architecture arch of shifter is

begin

	process (input, cnt)
		variable temp : std_logic_vector(15 downto 0) := (others => '0');
	begin
	temp := (others => '0');
		if cnt = "00" then
			for i in 7 downto 0 loop
				temp(i) := input(i);
			end loop;
		elsif cnt = "01" then
			for i in 11 downto 4 loop
				temp(i) := input(i-4);
			end loop;
		elsif cnt = "10" then
			for i in 15 downto 8 loop
				temp(i) := input(i-8);
		else 
			for i in 7 downto 0 loop
				temp(i) := input(i);
			end loop;
		end if;

		result <= temp;
	end process;
end arch;
