----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:57:25 10/23/2014 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
    Port ( clk : in  STD_LOGIC;
    			 rst : in std_logic;
           counter : out STD_LOGIC_VECTOR (7 downto 0));
end counter;

architecture Behavioral of counter is
	-- constant period : integer := 2499999;
	constant period : integer := 9999999;

	
	signal clk_s : std_logic := '0';
	signal i : integer range 0 to period := 0;
	signal counter_s : std_logic_vector(7 downto 0) := x"00";
	
begin

counter <= counter_s;


process (rst, clk)
begin
	if(clk'event and clk='1') then
		if (rst ='1') then
			clk_s <= '0';
			i <= 0;
			counter_s <= x"00";
		elsif (i = period) then
			i <= 0;
			clk_s <= not clk_s;
			if (counter_s < x"ff") then
				counter_s <= counter_s + 1;
			else
				counter_s <= x"00";
			end if;
		else
			i <= i+1;
		end if;
	end if;
end process;






end Behavioral;

