--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   08:56:28 10/22/2014
-- Design Name:   
-- Module Name:   C:/Users/Tiago Gomes/ufc/cursoVHDL/Tiago/2014/projetosIse/testeSoma/files/sum_tb.vhd
-- Project Name:  testeSoma
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sum
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY sum_tb IS
END sum_tb;
 
ARCHITECTURE behavior OF sum_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sum
    PORT(
         a : IN  std_logic_vector( 3 downto 0);
         b : IN  std_logic_vector( 3 downto 0);
         sum : OUT  std_logic_vector( 4 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a_s : std_logic_vector( 3 downto 0) := x"0";
   signal b_s : std_logic_vector( 3 downto 0) := x"0";

 	--Outputs
   signal sum_s : std_logic_vector( 4 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
--   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sum PORT MAP (
          a => a_s,
          b => b_s,
          sum => sum_s
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
		a_s <= x"8";
		b_s <= x"8";		
		wait for 1 ms;
		
		a_s <= x"a";
		b_s <= x"5";		
		wait for 1 ms;
				
      wait;
   end process;

END;
