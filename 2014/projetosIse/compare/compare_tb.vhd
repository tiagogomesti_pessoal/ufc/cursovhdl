--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:07:36 10/22/2014
-- Design Name:   
-- Module Name:   C:/Users/Tiago Gomes/ufc/cursoVHDL/Tiago/2014/projetosIse/compare/compare_tb.vhd
-- Project Name:  compare
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: compare
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY compare_tb IS
END compare_tb;
 
ARCHITECTURE behavior OF compare_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT compare
    PORT(
         A : IN  std_logic_vector(0 to 7);
         B : IN  std_logic_vector(0 to 7);
         EQ : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A_s : std_logic_vector(0 to 7) := (others => '0');
   signal B_s : std_logic_vector(0 to 7) := (others => '0');

 	--Outputs
   signal EQ_s : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: compare PORT MAP (
          A => A_s,
          B => B_s,
          EQ => EQ_s
        );

  -- Stimulus process
   stim_proc: process
   begin		
     A_s <= x"05";
	  B_s <= x"07";
	  
	  wait for 5 ms;
	  
	  A_s <= x"07";
	  B_s <= x"07";
	  
	  wait for 5 ms;
	  
	  A_s <= x"12";
	  B_s <= x"a3";
	  
	  wait for 5 ms;
	  
		
	  
   end process;

END;
