--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: compare_map.vhd
-- /___/   /\     Timestamp: Wed Oct 22 17:41:35 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -s 4 -pcf compare.pcf -rpw 100 -tpw 0 -ar Structure -tm compare -w -dir netgen/map -ofmt vhdl -sim compare_map.ncd compare_map.vhd 
-- Device	: 3s250etq144-4 (PRODUCTION 1.27 2013-10-13)
-- Input file	: compare_map.ncd
-- Output file	: C:\Users\Tiago Gomes\ufc\cursoVHDL\Tiago\2014\projetosIse\compare\netgen\map\compare_map.vhd
-- # of Entities	: 1
-- Design Name	: compare
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity compare is
  port (
    EQ : out STD_LOGIC; 
    A : in STD_LOGIC_VECTOR ( 0 to 7 ); 
    B : in STD_LOGIC_VECTOR ( 0 to 7 ) 
  );
end compare;

architecture Structure of compare is
  signal A_0_IBUF_76 : STD_LOGIC; 
  signal A_1_IBUF_77 : STD_LOGIC; 
  signal A_2_IBUF_78 : STD_LOGIC; 
  signal A_3_IBUF_79 : STD_LOGIC; 
  signal B_0_IBUF_81 : STD_LOGIC; 
  signal A_4_IBUF_82 : STD_LOGIC; 
  signal B_1_IBUF_83 : STD_LOGIC; 
  signal A_5_IBUF_84 : STD_LOGIC; 
  signal B_2_IBUF_85 : STD_LOGIC; 
  signal A_6_IBUF_86 : STD_LOGIC; 
  signal B_3_IBUF_87 : STD_LOGIC; 
  signal A_7_IBUF_88 : STD_LOGIC; 
  signal B_4_IBUF_89 : STD_LOGIC; 
  signal B_5_IBUF_90 : STD_LOGIC; 
  signal B_6_IBUF_91 : STD_LOGIC; 
  signal B_7_IBUF_92 : STD_LOGIC; 
  signal EQ826_0 : STD_LOGIC; 
  signal EQ853_0 : STD_LOGIC; 
  signal EQ893_0 : STD_LOGIC; 
  signal EQ8120_96 : STD_LOGIC; 
  signal A_0_INBUF : STD_LOGIC; 
  signal A_1_INBUF : STD_LOGIC; 
  signal A_2_INBUF : STD_LOGIC; 
  signal A_3_INBUF : STD_LOGIC; 
  signal EQ_O : STD_LOGIC; 
  signal B_0_INBUF : STD_LOGIC; 
  signal A_4_INBUF : STD_LOGIC; 
  signal B_1_INBUF : STD_LOGIC; 
  signal A_5_INBUF : STD_LOGIC; 
  signal B_2_INBUF : STD_LOGIC; 
  signal A_6_INBUF : STD_LOGIC; 
  signal B_3_INBUF : STD_LOGIC; 
  signal A_7_INBUF : STD_LOGIC; 
  signal B_4_INBUF : STD_LOGIC; 
  signal B_5_INBUF : STD_LOGIC; 
  signal B_6_INBUF : STD_LOGIC; 
  signal B_7_INBUF : STD_LOGIC; 
  signal EQ826_210 : STD_LOGIC; 
  signal EQ853_222 : STD_LOGIC; 
  signal EQ893_234 : STD_LOGIC; 
  signal EQ_OBUF_258 : STD_LOGIC; 
  signal EQ8120_pack_1 : STD_LOGIC; 
begin
  A_0_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(0),
      O => A_0_INBUF
    );
  A_1_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(1),
      O => A_1_INBUF
    );
  A_2_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(2),
      O => A_2_INBUF
    );
  A_2_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_2_INBUF,
      O => A_2_IBUF_78
    );
  A_3_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(3),
      O => A_3_INBUF
    );
  EQ_OBUF : X_OBUF
    port map (
      I => EQ_O,
      O => EQ
    );
  B_0_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(0),
      O => B_0_INBUF
    );
  A_4_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(4),
      O => A_4_INBUF
    );
  B_1_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(1),
      O => B_1_INBUF
    );
  A_5_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(5),
      O => A_5_INBUF
    );
  B_2_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(2),
      O => B_2_INBUF
    );
  B_2_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_2_INBUF,
      O => B_2_IBUF_85
    );
  A_6_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(6),
      O => A_6_INBUF
    );
  A_6_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_6_INBUF,
      O => A_6_IBUF_86
    );
  B_3_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(3),
      O => B_3_INBUF
    );
  A_7_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A(7),
      O => A_7_INBUF
    );
  B_4_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(4),
      O => B_4_INBUF
    );
  B_5_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(5),
      O => B_5_INBUF
    );
  B_6_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(6),
      O => B_6_INBUF
    );
  B_7_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B(7),
      O => B_7_INBUF
    );
  EQ826 : X_LUT4
    generic map(
      INIT => X"9009"
    )
    port map (
      ADR0 => A_4_IBUF_82,
      ADR1 => B_4_IBUF_89,
      ADR2 => A_5_IBUF_84,
      ADR3 => B_5_IBUF_90,
      O => EQ826_210
    );
  EQ826_XUSED : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => EQ826_210,
      O => EQ826_0
    );
  EQ853 : X_LUT4
    generic map(
      INIT => X"9009"
    )
    port map (
      ADR0 => A_6_IBUF_86,
      ADR1 => B_6_IBUF_91,
      ADR2 => A_7_IBUF_88,
      ADR3 => B_7_IBUF_92,
      O => EQ853_222
    );
  EQ853_XUSED : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => EQ853_222,
      O => EQ853_0
    );
  EQ893 : X_LUT4
    generic map(
      INIT => X"9009"
    )
    port map (
      ADR0 => A_0_IBUF_76,
      ADR1 => B_0_IBUF_81,
      ADR2 => A_1_IBUF_77,
      ADR3 => B_1_IBUF_83,
      O => EQ893_234
    );
  EQ893_XUSED : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => EQ893_234,
      O => EQ893_0
    );
  EQ_OBUF_YUSED : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => EQ8120_pack_1,
      O => EQ8120_96
    );
  A_0_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_0_INBUF,
      O => A_0_IBUF_76
    );
  A_1_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_1_INBUF,
      O => A_1_IBUF_77
    );
  A_3_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_3_INBUF,
      O => A_3_IBUF_79
    );
  B_3_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_3_INBUF,
      O => B_3_IBUF_87
    );
  B_0_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_0_INBUF,
      O => B_0_IBUF_81
    );
  A_4_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_4_INBUF,
      O => A_4_IBUF_82
    );
  B_1_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_1_INBUF,
      O => B_1_IBUF_83
    );
  A_5_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_5_INBUF,
      O => A_5_IBUF_84
    );
  A_7_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => A_7_INBUF,
      O => A_7_IBUF_88
    );
  B_4_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_4_INBUF,
      O => B_4_IBUF_89
    );
  B_5_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_5_INBUF,
      O => B_5_IBUF_90
    );
  B_6_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_6_INBUF,
      O => B_6_IBUF_91
    );
  B_7_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => B_7_INBUF,
      O => B_7_IBUF_92
    );
  EQ8120 : X_LUT4
    generic map(
      INIT => X"9009"
    )
    port map (
      ADR0 => A_2_IBUF_78,
      ADR1 => B_2_IBUF_85,
      ADR2 => A_3_IBUF_79,
      ADR3 => B_3_IBUF_87,
      O => EQ8120_pack_1
    );
  EQ8136 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => EQ826_0,
      ADR1 => EQ853_0,
      ADR2 => EQ893_0,
      ADR3 => EQ8120_96,
      O => EQ_OBUF_258
    );
  EQ_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => EQ_OBUF_258,
      O => EQ_O
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

