----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:05:50 10/20/2014 
-- Design Name: 
-- Module Name:    divisor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.std_logic_unsigned.all;
-- use IEEE.std_logic_arith.all;
use work.divisorPackage.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity divisor is
    Port ( dividend : in  vectorNbits;
           divisor : in  vectorNbits;
           quocient : out  vectorNbits;
           rest : out  vectorNbits
			 );
end divisor;

architecture Behavioral of divisor is


	signal quocient_s: vectorNbits;
	signal rest_s: vectorNbits;
	
	--signal divisor_extended: std_logic_vector(6 downto 0);
begin


	quocient <= quocient_s;
	rest <= rest_s;
	

	process(dividend,divisor)
		variable divisor_extended: vector_extented;
		variable dividend_v: vectorNbits;
		variable quocient_v: vectorNbits;
		
	begin
		divisor_extended(MSBExtended downto MSBVector) := divisor;
		divisor_extended(MSBVector-1 downto 0) := (others => '0');


		dividend_v := dividend;

		
		FOR i IN 0 to 3 LOOP
			if ( dividend_v < divisor_extended) then
				quocient_v(MSBVector-i) := '0';
			else
				dividend_v := dividend_v - divisor_extended(MSBVector downto 0);
				quocient_v(MSBVector-i) := '1';
			end if;
			
			divisor_extended(MSBExtended-1 downto 0) := divisor_extended(MSBExtended downto 1);
			divisor_extended(MSBExtended) := '0';
			
		END LOOP;		
		
		rest_s <= dividend_v;		
		quocient_s <= quocient_v;
		
	end process;

end Behavioral;















