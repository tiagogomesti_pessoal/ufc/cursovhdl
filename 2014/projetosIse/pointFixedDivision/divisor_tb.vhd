--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:39:28 10/20/2014
-- Design Name:   
-- Module Name:   C:/Users/Tiago Gomes/ufc/cursoVHDL/Tiago/2014/projetosIse/pointFixedDivision/divisor_tb.vhd
-- Project Name:  pointFixedDivision
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: divisor
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
use work.divisorPackage.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY divisor_tb IS
END divisor_tb;
 
ARCHITECTURE behavior OF divisor_tb IS 
 
   signal dividend_s : vectorNbits := (others => '0');
   signal divisor_s : vectorNbits := (others => '0');
   signal quocient_s : vectorNbits;
   signal rest_s : vectorNbits;

 
BEGIN

	divisor: entity work.divisor(Behavioral)
	port map
	(
		dividend => dividend_s,
		divisor => divisor_s,
		quocient => quocient_s,
		rest => rest_s
	);

	process
  begin		
		wait for 1 ms;	
	
		dividend_s	<= x"a";
		divisor_s	<= x"2";
     wait for 1 ms;	
		
		dividend_s	<= x"a";
		divisor_s	<= x"3";
     wait for 1 ms;	
		
		dividend_s	<= x"b";
		divisor_s	<= x"5";
     wait for 1 ms;	
		
		dividend_s	<= x"f";
		divisor_s	<= x"3";
   

     
  end process;

END behavior;
