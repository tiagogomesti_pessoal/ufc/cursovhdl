--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: divisor_synthesis.vhd
-- /___/   /\     Timestamp: Tue Oct 21 12:23:23 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm divisor -w -dir netgen/synthesis -ofmt vhdl -sim divisor.ngc divisor_synthesis.vhd 
-- Device	: xc3s250e-4-tq144
-- Input file	: divisor.ngc
-- Output file	: C:\Users\Tiago Gomes\ufc\cursoVHDL\Tiago\2014\projetosIse\testeBasys\netgen\synthesis\divisor_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: divisor
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity divisor is
  port (
    quocient : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    rest : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    dividend : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    divisor : in STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end divisor;

architecture Structure of divisor is
  signal N12 : STD_LOGIC; 
  signal N14 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal N20 : STD_LOGIC; 
  signal N22 : STD_LOGIC; 
  signal N7 : STD_LOGIC; 
  signal dividend_0_IBUF_12 : STD_LOGIC; 
  signal dividend_1_IBUF_13 : STD_LOGIC; 
  signal dividend_2_IBUF_14 : STD_LOGIC; 
  signal dividend_3_IBUF_15 : STD_LOGIC; 
  signal dividend_v_cmp_lt0000 : STD_LOGIC; 
  signal dividend_v_cmp_lt0002 : STD_LOGIC; 
  signal dividend_v_mux0001_2_1 : STD_LOGIC; 
  signal dividend_v_mux0002_2_1 : STD_LOGIC; 
  signal dividend_v_mux0002_2_11_25 : STD_LOGIC; 
  signal divisor_0_IBUF_31 : STD_LOGIC; 
  signal divisor_1_IBUF_32 : STD_LOGIC; 
  signal divisor_2_IBUF_33 : STD_LOGIC; 
  signal divisor_3_IBUF_34 : STD_LOGIC; 
  signal quocient_0_OBUF_39 : STD_LOGIC; 
  signal quocient_1_OBUF_40 : STD_LOGIC; 
  signal quocient_2_OBUF_41 : STD_LOGIC; 
  signal quocient_3_OBUF_42 : STD_LOGIC; 
  signal quocient_v_0_cmp_lt0000 : STD_LOGIC; 
  signal quocient_v_0_cmp_lt00001_44 : STD_LOGIC; 
  signal rest_0_OBUF_49 : STD_LOGIC; 
  signal rest_1_OBUF_50 : STD_LOGIC; 
  signal rest_2_OBUF_51 : STD_LOGIC; 
  signal rest_3_OBUF_52 : STD_LOGIC; 
  signal rest_s_1_1 : STD_LOGIC; 
  signal Msub_dividend_v_addsub0002_cy : STD_LOGIC_VECTOR ( 2 downto 2 ); 
  signal Msub_rest_s_addsub0000_cy : STD_LOGIC_VECTOR ( 1 downto 1 ); 
  signal dividend_v_mux0000 : STD_LOGIC_VECTOR ( 3 downto 3 ); 
  signal dividend_v_mux0001 : STD_LOGIC_VECTOR ( 3 downto 2 ); 
  signal dividend_v_mux0002 : STD_LOGIC_VECTOR ( 3 downto 1 ); 
begin
  dividend_v_mux0002_3_1 : LUT4
    generic map(
      INIT => X"AA69"
    )
    port map (
      I0 => dividend_v_mux0001(3),
      I1 => Msub_dividend_v_addsub0002_cy(2),
      I2 => divisor_2_IBUF_33,
      I3 => dividend_v_cmp_lt0002,
      O => dividend_v_mux0002(3)
    );
  rest_s_0_1 : LUT3
    generic map(
      INIT => X"A6"
    )
    port map (
      I0 => dividend_0_IBUF_12,
      I1 => divisor_0_IBUF_31,
      I2 => quocient_v_0_cmp_lt0000,
      O => rest_0_OBUF_49
    );
  dividend_v_mux0002_1_1 : LUT3
    generic map(
      INIT => X"A6"
    )
    port map (
      I0 => dividend_1_IBUF_13,
      I1 => divisor_0_IBUF_31,
      I2 => dividend_v_cmp_lt0002,
      O => dividend_v_mux0002(1)
    );
  dividend_v_mux0000_3_1 : LUT3
    generic map(
      INIT => X"A6"
    )
    port map (
      I0 => dividend_3_IBUF_15,
      I1 => divisor_0_IBUF_31,
      I2 => dividend_v_cmp_lt0000,
      O => dividend_v_mux0000(3)
    );
  quocient_v_0_cmp_lt00002 : LUT4
    generic map(
      INIT => X"20BA"
    )
    port map (
      I0 => divisor_1_IBUF_32,
      I1 => dividend_0_IBUF_12,
      I2 => divisor_0_IBUF_31,
      I3 => dividend_v_mux0002(1),
      O => quocient_v_0_cmp_lt00001_44
    );
  dividend_v_cmp_lt00001_SW0 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => divisor_0_IBUF_31,
      I1 => dividend_3_IBUF_15,
      O => N7
    );
  dividend_v_cmp_lt00001 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => divisor_3_IBUF_34,
      I1 => divisor_2_IBUF_33,
      I2 => divisor_1_IBUF_32,
      I3 => N7,
      O => dividend_v_cmp_lt0000
    );
  dividend_v_cmp_lt00011_SW0 : LUT4
    generic map(
      INIT => X"20BA"
    )
    port map (
      I0 => divisor_1_IBUF_32,
      I1 => dividend_2_IBUF_14,
      I2 => divisor_0_IBUF_31,
      I3 => dividend_v_mux0000(3),
      O => N12
    );
  dividend_v_cmp_lt00021_SW0 : LUT4
    generic map(
      INIT => X"20BA"
    )
    port map (
      I0 => divisor_1_IBUF_32,
      I1 => dividend_1_IBUF_13,
      I2 => divisor_0_IBUF_31,
      I3 => dividend_v_mux0001(2),
      O => N14
    );
  dividend_v_cmp_lt00021 : LUT4
    generic map(
      INIT => X"FF8E"
    )
    port map (
      I0 => N14,
      I1 => divisor_2_IBUF_33,
      I2 => dividend_v_mux0001(3),
      I3 => divisor_3_IBUF_34,
      O => dividend_v_cmp_lt0002
    );
  dividend_3_IBUF : IBUF
    port map (
      I => dividend(3),
      O => dividend_3_IBUF_15
    );
  dividend_2_IBUF : IBUF
    port map (
      I => dividend(2),
      O => dividend_2_IBUF_14
    );
  dividend_1_IBUF : IBUF
    port map (
      I => dividend(1),
      O => dividend_1_IBUF_13
    );
  dividend_0_IBUF : IBUF
    port map (
      I => dividend(0),
      O => dividend_0_IBUF_12
    );
  divisor_3_IBUF : IBUF
    port map (
      I => divisor(3),
      O => divisor_3_IBUF_34
    );
  divisor_2_IBUF : IBUF
    port map (
      I => divisor(2),
      O => divisor_2_IBUF_33
    );
  divisor_1_IBUF : IBUF
    port map (
      I => divisor(1),
      O => divisor_1_IBUF_32
    );
  divisor_0_IBUF : IBUF
    port map (
      I => divisor(0),
      O => divisor_0_IBUF_31
    );
  quocient_3_OBUF : OBUF
    port map (
      I => quocient_3_OBUF_42,
      O => quocient(3)
    );
  quocient_2_OBUF : OBUF
    port map (
      I => quocient_2_OBUF_41,
      O => quocient(2)
    );
  quocient_1_OBUF : OBUF
    port map (
      I => quocient_1_OBUF_40,
      O => quocient(1)
    );
  quocient_0_OBUF : OBUF
    port map (
      I => quocient_0_OBUF_39,
      O => quocient(0)
    );
  rest_3_OBUF : OBUF
    port map (
      I => rest_3_OBUF_52,
      O => rest(3)
    );
  rest_2_OBUF : OBUF
    port map (
      I => rest_2_OBUF_51,
      O => rest(2)
    );
  rest_1_OBUF : OBUF
    port map (
      I => rest_1_OBUF_50,
      O => rest(1)
    );
  rest_0_OBUF : OBUF
    port map (
      I => rest_0_OBUF_49,
      O => rest(0)
    );
  quocient_v_0_cmp_lt00001_SW2 : LUT3
    generic map(
      INIT => X"D4"
    )
    port map (
      I0 => dividend_v_mux0002(2),
      I1 => quocient_v_0_cmp_lt00001_44,
      I2 => divisor_2_IBUF_33,
      O => N16
    );
  quocient_v_0_cmp_lt00001 : LUT3
    generic map(
      INIT => X"D4"
    )
    port map (
      I0 => dividend_v_mux0002(3),
      I1 => divisor_3_IBUF_34,
      I2 => N16,
      O => quocient_v_0_cmp_lt0000
    );
  Msub_dividend_v_addsub0002_cy_2_11 : LUT4
    generic map(
      INIT => X"CF4D"
    )
    port map (
      I0 => divisor_0_IBUF_31,
      I1 => dividend_v_mux0001(2),
      I2 => divisor_1_IBUF_32,
      I3 => dividend_1_IBUF_13,
      O => Msub_dividend_v_addsub0002_cy(2)
    );
  Msub_rest_s_addsub0000_cy_1_11 : LUT4
    generic map(
      INIT => X"CF4D"
    )
    port map (
      I0 => divisor_0_IBUF_31,
      I1 => dividend_v_mux0002(1),
      I2 => divisor_1_IBUF_32,
      I3 => dividend_0_IBUF_12,
      O => Msub_rest_s_addsub0000_cy(1)
    );
  rest_s_2_1 : LUT4
    generic map(
      INIT => X"AA69"
    )
    port map (
      I0 => dividend_v_mux0002(2),
      I1 => Msub_rest_s_addsub0000_cy(1),
      I2 => divisor_2_IBUF_33,
      I3 => quocient_v_0_cmp_lt0000,
      O => rest_2_OBUF_51
    );
  quocient_v_3_mux00001 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => divisor_3_IBUF_34,
      I1 => divisor_2_IBUF_33,
      I2 => divisor_1_IBUF_32,
      I3 => N7,
      O => quocient_3_OBUF_42
    );
  quocient_v_2_mux00001 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => divisor_3_IBUF_34,
      I1 => divisor_2_IBUF_33,
      I2 => N12,
      O => quocient_2_OBUF_41
    );
  quocient_v_1_mux00001 : LUT4
    generic map(
      INIT => X"040D"
    )
    port map (
      I0 => divisor_2_IBUF_33,
      I1 => dividend_v_mux0001(3),
      I2 => divisor_3_IBUF_34,
      I3 => N14,
      O => quocient_1_OBUF_40
    );
  dividend_v_mux0001_3_1_SW1 : LUT3
    generic map(
      INIT => X"65"
    )
    port map (
      I0 => divisor_1_IBUF_32,
      I1 => dividend_2_IBUF_14,
      I2 => divisor_0_IBUF_31,
      O => N20
    );
  dividend_v_mux0001_3_1 : LUT4
    generic map(
      INIT => X"AAA8"
    )
    port map (
      I0 => dividend_v_mux0000(3),
      I1 => divisor_2_IBUF_33,
      I2 => divisor_3_IBUF_34,
      I3 => N20,
      O => dividend_v_mux0001(3)
    );
  rest_s_3_1_SW0 : LUT3
    generic map(
      INIT => X"D4"
    )
    port map (
      I0 => divisor_2_IBUF_33,
      I1 => dividend_v_mux0002(2),
      I2 => Msub_rest_s_addsub0000_cy(1),
      O => N22
    );
  rest_s_3_1 : LUT4
    generic map(
      INIT => X"A829"
    )
    port map (
      I0 => dividend_v_mux0002(3),
      I1 => divisor_3_IBUF_34,
      I2 => N22,
      I3 => N16,
      O => rest_3_OBUF_52
    );
  quocient_v_0_mux00001 : LUT3
    generic map(
      INIT => X"4D"
    )
    port map (
      I0 => divisor_3_IBUF_34,
      I1 => dividend_v_mux0002(3),
      I2 => N16,
      O => quocient_0_OBUF_39
    );
  dividend_v_mux0001_2_11 : LUT4
    generic map(
      INIT => X"A9AA"
    )
    port map (
      I0 => dividend_2_IBUF_14,
      I1 => divisor_2_IBUF_33,
      I2 => divisor_3_IBUF_34,
      I3 => divisor_0_IBUF_31,
      O => dividend_v_mux0001_2_1
    );
  dividend_v_mux0001_2_1_f5 : MUXF5
    port map (
      I0 => dividend_v_mux0001_2_1,
      I1 => dividend_2_IBUF_14,
      S => N12,
      O => dividend_v_mux0001(2)
    );
  dividend_v_mux0002_2_11 : LUT4
    generic map(
      INIT => X"9C99"
    )
    port map (
      I0 => dividend_v_cmp_lt0002,
      I1 => dividend_v_mux0001(2),
      I2 => dividend_1_IBUF_13,
      I3 => divisor_0_IBUF_31,
      O => dividend_v_mux0002_2_1
    );
  dividend_v_mux0002_2_12 : LUT4
    generic map(
      INIT => X"A9AA"
    )
    port map (
      I0 => dividend_v_mux0001(2),
      I1 => dividend_1_IBUF_13,
      I2 => dividend_v_cmp_lt0002,
      I3 => divisor_0_IBUF_31,
      O => dividend_v_mux0002_2_11_25
    );
  dividend_v_mux0002_2_1_f5 : MUXF5
    port map (
      I0 => dividend_v_mux0002_2_11_25,
      I1 => dividend_v_mux0002_2_1,
      S => divisor_1_IBUF_32,
      O => dividend_v_mux0002(2)
    );
  rest_s_1_11 : LUT4
    generic map(
      INIT => X"5A96"
    )
    port map (
      I0 => dividend_v_mux0002(1),
      I1 => divisor_0_IBUF_31,
      I2 => divisor_1_IBUF_32,
      I3 => dividend_0_IBUF_12,
      O => rest_s_1_1
    );
  rest_s_1_1_f5 : MUXF5
    port map (
      I0 => rest_s_1_1,
      I1 => dividend_v_mux0002(1),
      S => quocient_v_0_cmp_lt0000,
      O => rest_1_OBUF_50
    );

end Structure;

