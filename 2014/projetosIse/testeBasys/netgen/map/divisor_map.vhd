--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: divisor_map.vhd
-- /___/   /\     Timestamp: Tue Oct 21 13:34:38 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -s 4 -pcf divisor.pcf -rpw 100 -tpw 0 -ar Structure -tm divisor -w -dir netgen/map -ofmt vhdl -sim divisor_map.ncd divisor_map.vhd 
-- Device	: 3s250etq144-4 (PRODUCTION 1.27 2013-10-13)
-- Input file	: divisor_map.ncd
-- Output file	: C:\Users\Tiago Gomes\ufc\cursoVHDL\Tiago\2014\projetosIse\testeBasys\netgen\map\divisor_map.vhd
-- # of Entities	: 1
-- Design Name	: divisor
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity divisor is
  port (
    quocient : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    rest : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dividend : in STD_LOGIC_VECTOR ( 1 downto 0 ); 
    divisor : in STD_LOGIC_VECTOR ( 1 downto 0 ) 
  );
end divisor;

architecture Structure of divisor is
  signal divisor_0_IBUF_35 : STD_LOGIC; 
  signal divisor_1_IBUF_36 : STD_LOGIC; 
  signal dividend_1_IBUF_41 : STD_LOGIC; 
  signal divisor_0_INBUF : STD_LOGIC; 
  signal divisor_1_INBUF : STD_LOGIC; 
  signal rest_0_O : STD_LOGIC; 
  signal rest_1_O : STD_LOGIC; 
  signal quocient_0_O : STD_LOGIC; 
  signal quocient_1_O : STD_LOGIC; 
  signal dividend_0_INBUF : STD_LOGIC; 
  signal dividend_1_INBUF : STD_LOGIC; 
  signal quocient_1_OBUF_119 : STD_LOGIC; 
  signal rest_1_OBUF_111 : STD_LOGIC; 
  signal quocient_0_OBUF_131 : STD_LOGIC; 
  signal VCC : STD_LOGIC; 
begin
  divisor_0_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => divisor(0),
      O => divisor_0_INBUF
    );
  divisor_1_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => divisor(1),
      O => divisor_1_INBUF
    );
  rest_0_OBUF : X_OBUF
    port map (
      I => rest_0_O,
      O => rest(0)
    );
  rest_1_OBUF : X_OBUF
    port map (
      I => rest_1_O,
      O => rest(1)
    );
  quocient_0_OBUF : X_OBUF
    port map (
      I => quocient_0_O,
      O => quocient(0)
    );
  quocient_1_OBUF : X_OBUF
    port map (
      I => quocient_1_O,
      O => quocient(1)
    );
  dividend_0_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => dividend(0),
      O => dividend_0_INBUF
    );
  dividend_1_IBUF : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => dividend(1),
      O => dividend_1_INBUF
    );
  divisor_0_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => divisor_0_INBUF,
      O => divisor_0_IBUF_35
    );
  divisor_1_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => divisor_1_INBUF,
      O => divisor_1_IBUF_36
    );
  dividend_1_IFF_IMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => dividend_1_INBUF,
      O => dividend_1_IBUF_41
    );
  rest_1_1 : X_LUT4
    generic map(
      INIT => X"A2A2"
    )
    port map (
      ADR0 => dividend_1_IBUF_41,
      ADR1 => divisor_0_IBUF_35,
      ADR2 => divisor_1_IBUF_36,
      ADR3 => VCC,
      O => rest_1_OBUF_111
    );
  quocient_1_1 : X_LUT4
    generic map(
      INIT => X"4545"
    )
    port map (
      ADR0 => divisor_1_IBUF_36,
      ADR1 => dividend_1_IBUF_41,
      ADR2 => divisor_0_IBUF_35,
      ADR3 => VCC,
      O => quocient_1_OBUF_119
    );
  quocient_0_1 : X_LUT4
    generic map(
      INIT => X"1111"
    )
    port map (
      ADR0 => divisor_1_IBUF_36,
      ADR1 => divisor_0_IBUF_35,
      ADR2 => VCC,
      ADR3 => VCC,
      O => quocient_0_OBUF_131
    );
  rest_0_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => dividend_0_INBUF,
      O => rest_0_O
    );
  rest_1_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => rest_1_OBUF_111,
      O => rest_1_O
    );
  quocient_0_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => quocient_0_OBUF_131,
      O => quocient_0_O
    );
  quocient_1_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      PATHPULSE => 798 ps
    )
    port map (
      I => quocient_1_OBUF_119,
      O => quocient_1_O
    );
  NlwBlock_divisor_VCC : X_ONE
    port map (
      O => VCC
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

