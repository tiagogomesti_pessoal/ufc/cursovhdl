--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

package divisorPackage is

	constant nBits: integer := 4;
	constant MSBVector: integer := nBits-1;
	constant MSBExtended: integer := 2*nBits-2;


	subtype vectorNbits is std_logic_vector(MSBVector downto 0);
	subtype vector_extented is std_logic_vector(MSBExtended downto 0);


end divisorPackage;

package body divisorPackage is

end divisorPackage;
